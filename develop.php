<?php
interface GPS {
    public function charecteristic($choice, $name);
}
class Taxi implements GPS {
    public function charecteristic($choice, $name) {
        echo 'Вы выбрали Такси,приятной поездки' . $name . "!";
    }
}
class Bus implements GPS {
    public function charecteristic($choice, $name) {
        echo 'В тесноте та не в обиде, приятной поездки ' .  $name . "!";
    }
}
class Plane implements GPS {
    public function charecteristic($choice, $name) {
        echo 'Вы выбрали самолёт, приятного полёта ' .  $name . "!";
    }
}
class GpsSender {
    private $strategy;

    public function setStrategy(GPS $nameUser) {
        return $this->strategy = $nameUser;
    }
    public function takeName($takeNAME) {
        $this->strategy = $takeNAME;
    }
    public function executeStrategy($executeVar) {
        if($executeVar === 'Taxi') {
            return Taxi::charecteristic($executeVar,$this->strategy);
        }
        if($executeVar === 'Plane') {
            return Plane::charecteristic($executeVar, $this->strategy);
        }
        if($executeVar === 'Bus') {
            return Bus::charecteristic($executeVar, $this->strategy);
        }
    }
}
$user = new GpsSender();
$user->takeName('Михаил');
$user->executeStrategy('Bus');
?>